package br.edu.up.bigdata;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Delete;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.SubstringComparator;
import org.apache.hadoop.hbase.filter.ValueFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.stereotype.Service;

@Service
public class HBaseDAO {

	private Connection connection;
	
	
	public HBaseDAO() throws Exception {
		Configuration config = HBaseConfiguration.create();
		
		String path = this.getClass()
		  .getClassLoader()
		  .getResource("hbase-site.xml")
		  .getPath();
		
		connection = ConnectionFactory.createConnection(config);
		
		//HBaseAdmin.available(config);
		//Admin admin = connection.getAdmin();
	}
	
	
	public String scan(String tableName) throws Exception {
		
		Table table = connection.getTable(TableName.valueOf(tableName));
		
		Scan scan = new Scan();
		ResultScanner scanner = table.getScanner(scan);
		
		return createResultMessage(scanner);
	}
	
	public String scanRowFilter(String tableName, String rowPrefix) throws Exception {
		
		
		Table table = connection.getTable(TableName.valueOf(tableName));
		
		Scan scan = new Scan();
		scan.setRowPrefixFilter(Bytes.toBytes(rowPrefix));
		ResultScanner scanner = table.getScanner(scan);
		
		return createResultMessage(scanner);
	}


	public String scanValueFilter(String tableName, String valueFilter) throws Exception {
		
		Table table = connection.getTable(TableName.valueOf(tableName));
		
		Scan scan = new Scan();
		Filter filter = new ValueFilter(CompareOp.EQUAL, new SubstringComparator(valueFilter));
		scan.setFilter(filter);
		
		ResultScanner scanner = table.getScanner(scan);
		
		return createResultMessage(scanner);
	}
	
	public String get(String tableName, String rowId) throws Exception {
		StringBuffer retorno = new StringBuffer();
		
		Table table = connection.getTable(TableName.valueOf(tableName));

		Result result = table.get(new Get(rowId.getBytes()));
		List<Cell> listCells = result.listCells();
		for (Cell cell : listCells) {
			retorno.append("\n rowId= "+Bytes.toString(cell.getRow()));
			retorno.append(" "+Bytes.toString(cell.getFamily()));
			retorno.append(":"+Bytes.toString(cell.getQualifier()));
			retorno.append(" value= "+Bytes.toString(cell.getValue()));
		}
		
		retorno.append("\n");
		return retorno.toString();
	}

	public void put(String tableName, String rowId, String family, String qualifier, String value) throws Exception {
		Table table = connection.getTable(TableName.valueOf(tableName));
		Put put = new Put(Bytes.toBytes(rowId));
		put.addColumn(Bytes.toBytes(family), Bytes.toBytes(qualifier), Bytes.toBytes(value));
		table.put(put );
	}
	
	public void delete(String tableName, String rowId, String family, String qualifier) throws Exception {
		Table table = connection.getTable(TableName.valueOf(tableName));
		Delete delete = new Delete(Bytes.toBytes(rowId));
		
		if(StringUtils.isNotBlank(family) && StringUtils.isNotBlank(qualifier) ) {
			delete.addColumn(Bytes.toBytes(family), Bytes.toBytes(qualifier));
		}
		
		table.delete(delete);
	}
	
	private String createResultMessage(ResultScanner scanner) {
		StringBuffer retorno = new StringBuffer("\n");
		for (Result result : scanner) {
			List<Cell> listCells = result.listCells();
			for (Cell cell : listCells) {
				retorno.append(" rowId= "+Bytes.toString(cell.getRow()));
				retorno.append(" "+Bytes.toString(cell.getFamily()));
				retorno.append(":"+Bytes.toString(cell.getQualifier()));
				retorno.append(" value= "+Bytes.toString(cell.getValue()));
			}
			retorno.append("\n");
		}
		return retorno.toString();
	}
	
	
}
