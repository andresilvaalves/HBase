# HBase

## Java API HBase

#### BIG DATA Postgraduation Course <br> Universidade Positivo - Curitiba - Brazil <br> 

## comands

`$ java -jar HBase-0.1.0.jar`




###### -- scan 
`$ curl http://localhost:8081/scan/{tableName}`

###### -- scan RowFilter
`$ curl http://localhost:8081/scanRowFilter/{tableName}/{rowPrefix}`

###### scan valueFilter
`$ curl http://localhost:8081/scanValueFilter/{tableName}/{valueFilter}`

###### -- get
`$ curl http://localhost:8081/get/{tableName}/{rowId}`

###### -- put
`$ curl -X PUT http://localhost:8081/put/{tableName}/{rowId}/{family}/{qualifier}/{value}`

###### -- delete 
`$ curl -X DELETE http://localhost:8081/delete/{tableName}/{rowId}`

###### -- delete 
`$ curl -X DELETE http://localhost:8081/delete/{tableName}/{rowId}/{family}/{qualifier}`


